<?php

class User
{
    private $id;

    /**
     * User constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}

class Subscription
{


    /**
     * @var SubscriptionPlan
     */
    private $plan;

    /**
     * @var integer
     */
    private $gatewayId;

    /**
     * Subscription constructor.
     * @param $planId int
     */
    public function __construct($planId)
    {
        $this->plan = new SubscriptionPlan($planId);
        $this->gatewayId = $planId;
    }


    /**
     * @return SubscriptionPlan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * @return int
     */
    public function getGatewayId()
    {
        return $this->gatewayId;
    }
}

class SubscriptionPlan
{
    const FREE = 1;
    const PAID = 2;

    private $id;

    /**
     * SubscriptionPlan constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}

class SubscriptionRepository
{
    public function getActiveSubscription(User $user)
    {
        // emulate case
        if ($user->getId() % 10) {
            // Just a stub
            return new Subscription($user->getId());
        } else {
            return null;
        }
    }
}

class SubscriptionGatewayApi
{

    /**
     * @param $id
     * @throws Exception
     */
    public function cancel($id)
    {
        if ($id % 101 == 0) {
            throw new Exception();
        }
    }
}

class Response
{
    /**
     * @var integer
     */
    private $code;

    private $data;

    /**
     * Response constructor.
     * @param int $code
     * @param $data
     */
    public function __construct($code, $data = [])
    {
        $this->code = $code;
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }
}

class SubscriptionService
{
    const FAILED_CANCEL_SUBSCRIPTION_KEY = 1;
    const NOT_FOUND_ACTIVE_SUBSCRIPTION_KEY = 2;

    /**
     * @var SubscriptionRepository
     */
    private $subscriptionRepository;

    /**
     * @var SubscriptionGatewayApi
     */
    private $subscriptionGatewayApi;

    /**
     * SubscriptionService constructor.
     */
    public function __construct()
    {
        $this->subscriptionRepository = new SubscriptionRepository();
        $this->subscriptionGatewayApi = new SubscriptionGatewayApi();
    }


    /**
     * @param $user
     *
     * @return array
     * @throws Exception
     */
    public function cancelSubscription($user)
    {
        $message            = [];
        $activeSubscription = $this->subscriptionRepository->getActiveSubscription($user);
        if ( ! empty($activeSubscription)) {
            try {
                $this->subscriptionGatewayApi->cancel($activeSubscription->getGatewayId());
            } catch (\Exception $exception) {
                throw new Exception(self::FAILED_CANCEL_SUBSCRIPTION_KEY);
            }

        } else {
            throw new Exception(self::NOT_FOUND_ACTIVE_SUBSCRIPTION_KEY);
        }

        return $message;
    }
}

class SubscriptionController
{
    private $subscriptionService;

    /**
     * SubscriptionController constructor.
     */
    public function __construct()
    {
        $this->subscriptionService = new SubscriptionService();
    }


    public function cancelSubscriptionForUser($userId)
    {
        $user = new User($userId);
        try {
            $result = $this->subscriptionService->cancelSubscription($user);
            if ( ! $result) {
                return new Response(200);
            }
        } catch (\Exception $exception) {
            //http_response_code($result['code']);
            if ($exception->getMessage() == SubscriptionService::FAILED_CANCEL_SUBSCRIPTION_KEY) {
                return new Response(422, [
                    'message' => 'The subscription wasn\'t cancelled due to technical issue'
                ]);
            }
            if ($exception->getMessage() == SubscriptionService::NOT_FOUND_ACTIVE_SUBSCRIPTION_KEY) {
                return new Response(404, [
                    'message' => 'Subscription is not found for user'
                ]);
            }
        }

        return null;
    }
}