<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 11.03.2019
 * Time: 12:44
 */
spl_autoload_register(function ($class_name) {

    $directory = 'Syberry/Academy/';
    $sub_directory = array('Controller/', 'Service/', 'Repository/',
        'Data/', 'Data/Subscription/',
        'Infrastructure/Http/', 'Infrastructure/Subscription/',);

//list of comma separated file format
    $fileFormat = array('%s.php', '%s.class.php');

    foreach ($sub_directory as $current_dir)
    {
        foreach ($fileFormat as $current_format)
        {
            $path = $directory.$current_dir.sprintf($current_format, $class_name);
            if (file_exists($path))
            {
                include $path;
                return ;
            }
        }
    }
});

/*
if (defined('STDIN')) {
    $userId = $argv[1];
} else {
    $userId = $_GET['userId'];
}
*/
if ($argc != 1) {
    $userId                 = $argv[1];
    $SubscriptionController = new SubscriptionController();
    $Response               = $SubscriptionController->cancelSubscriptionForUser($userId);
    $ResponseCode           = $Response->getCode();
    $ResponseCodeType       = intdiv($ResponseCode, 100);
    if ($ResponseCodeType == 2) {
        echo 'Success ' . $Response->getCode();
    }
    if ($ResponseCodeType == 4) {
        echo 'Error ' . $Response->getCode() . ': ' . $Response->getData()['message'];
    }
}
else {
    echo 'Parameter $userId not defined';
}
