<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 11.03.2019
 * Time: 12:36
 */
class SubscriptionRepository
{
    public function getActiveSubscription(User $user)
    {
        // emulate case
        if ($user->getId() % 10) {
            // Just a stub
            return new Subscription($user->getId());
        } else {
            return null;
        }
    }
}
