<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 11.03.2019
 * Time: 12:31
 */
class Subscription
{


    /**
     * @var SubscriptionPlan
     */
    private $plan;

    /**
     * @var integer
     */
    private $gatewayId;

    /**
     * Subscription constructor.
     * @param $planId int
     */
    public function __construct($planId)
    {
        $this->plan = new SubscriptionPlan($planId);
        $this->gatewayId = $planId;
    }


    /**
     * @return SubscriptionPlan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * @return int
     */
    public function getGatewayId()
    {
        return $this->gatewayId;
    }
}