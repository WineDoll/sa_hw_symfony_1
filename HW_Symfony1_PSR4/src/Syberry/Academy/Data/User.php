<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 11.03.2019
 * Time: 12:30
 */
class User
{
    private $id;

    /**
     * User constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}