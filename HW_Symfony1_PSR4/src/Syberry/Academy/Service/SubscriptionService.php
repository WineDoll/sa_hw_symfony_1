<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 11.03.2019
 * Time: 12:35
 */

class SubscriptionService
{
    const FAILED_CANCEL_SUBSCRIPTION_KEY = 1;
    const NOT_FOUND_ACTIVE_SUBSCRIPTION_KEY = 2;

    /**
     * @var SubscriptionRepository
     */
    private $subscriptionRepository;

    /**
     * @var SubscriptionGatewayApi
     */
    private $subscriptionGatewayApi;

    /**
     * SubscriptionService constructor.
     */
    public function __construct()
    {
        $this->subscriptionRepository = new SubscriptionRepository();
        $this->subscriptionGatewayApi = new SubscriptionGatewayApi();
    }


    /**
     * @param $user
     *
     * @return array
     * @throws Exception
     */
    public function cancelSubscription($user)
    {
        $message            = [];
        $activeSubscription = $this->subscriptionRepository->getActiveSubscription($user);
        if ( ! empty($activeSubscription)) {
            try {
                $this->subscriptionGatewayApi->cancel($activeSubscription->getGatewayId());
            } catch (\Exception $exception) {
                throw new Exception(self::FAILED_CANCEL_SUBSCRIPTION_KEY);
            }

        } else {
            throw new Exception(self::NOT_FOUND_ACTIVE_SUBSCRIPTION_KEY);
        }

        return $message;
    }
}
