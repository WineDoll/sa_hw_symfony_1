<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 11.03.2019
 * Time: 12:33
 */

class SubscriptionController
{
    private $subscriptionService;

    /**
     * SubscriptionController constructor.
     */
    public function __construct()
    {
        $this->subscriptionService = new SubscriptionService();
    }


    public function cancelSubscriptionForUser($userId)
    {
        $user = new User($userId);
        try {
            $result = $this->subscriptionService->cancelSubscription($user);
            if ( ! $result) {
                return new Response(200);
            }
        } catch (\Exception $exception) {
            //http_response_code($result['code']);
            if ($exception->getMessage() == SubscriptionService::FAILED_CANCEL_SUBSCRIPTION_KEY) {
                return new Response(422, [
                    'message' => 'The subscription wasn\'t cancelled due to technical issue'
                ]);
            }
            if ($exception->getMessage() == SubscriptionService::NOT_FOUND_ACTIVE_SUBSCRIPTION_KEY) {
                return new Response(404, [
                    'message' => 'Subscription is not found for user'
                ]);
            }
        }

        return null;
    }
}
